<?php

namespace App\Http\Controllers;

use App\Article;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ArticleController extends Controller
{
     /**
     * Display a listing of the articles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try {
            $articles = Article::all();
            return ApiHelper::jsonSuccess($articles);
          } catch (\Exception $e) {
            return ApiHelper::jsonError(['message' => 'Error']);
          }
    }
}
