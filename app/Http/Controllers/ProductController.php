<?php

namespace App\Http\Controllers;

use App\Product;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
        /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
       
        try {
            $products = Product::all();
            return ApiHelper::jsonSuccess($products);
          } catch (\Exception $e) {
            return ApiHelper::jsonError(['message' => 'Error']);
          }
    }

     /**
     * Store a newly created product in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
        try {
           $rules = [
              'titleEn' => 'required',
              'descriptionEn' => 'required',
              'price' => 'required',
              'active' => 'required',
            ];
  
            $validator = Validator::make($request->all(), $rules);
  
            if ($validator->fails()) {
                return ApiHelper::jsonError(['message' => $validator->errors()]);
            } else {
              $product = new Product();
              $product->titleEn = $request->titleEn;
              $product->titleDe = $request->titleDe;
              $product->descriptionEn = $request->descriptionEn;
              $product->descriptionDe = $request->descriptionDe;
              $product->price = $request->price;
              $product->active = $request->active;
  
              $product->save();
  
              return ApiHelper::jsonSuccess(['message' => "Product Saved"]);

              
            }
  
          } catch (\Exception $e) {
           return ApiHelper::jsonError(['message' => "Error"]);
           
          }
    }
}
