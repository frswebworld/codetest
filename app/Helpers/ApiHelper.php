<?php
namespace App\Helpers;

class ApiHelper
{

  public static function jsonSuccess($data, $status_code = 201)
  {
    return response()->json([
      'status' => 'success',
      'status_code' => $status_code,
      'data' => $data
    ],$status_code);
  }

  public static function jsonFail($data, $status_code = 406)
  {
    return response()->json([
      'status' => 'fail',
      'status_code' => $status_code,
      'errors' => $data
    ],$status_code);
  }

  public static function jsonError($data, $status_code = 501)
  {
    return response()->json([
      'status' => 'error',
      'status_code' => $status_code,
      'errors' => $data
    ],$status_code);
  }
}


?>
