<?php

use App\Article;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
     /**
     * Seed the application's database.
     * Here json data are being imported from data.json and stored it into database
     * @return void
     */
    public function run()
    {
         //$this->call(UsersTableSeeder::class);
        $json = file_get_contents(storage_path('data.json'));
        $objs = json_decode($json,true);
        foreach ($objs as $obj)  {
           foreach ($obj as $key => $value) {
               $article = new Article();
                $article->article_number = $value['article_number'];
                $article->manufacturer_id = $value['manufacturer']['id'];
                $article->manufacturer_name = $value['manufacturer']['name'];
                $article->text1_de=$value['text1_de'];
                $article->text2_en=$value['text2_en'];
                $article->sku=$value['sku'];
                $article->created=$value['created'];
                $article->shop_status=$value['shop_status'];
                $article->hidden=$value['hidden'];
                $article->deleted=$value['deleted'];
                $article->weight=$value['weight'];
                $article->text2_de=$value['text2_de'];
                $article->text2_en=$value['text2_en'];
                $article->ean=$value['ean'];
                $article->ek=$value['ek'];
                $article->vk1=$value['vk1'];
                $article->vk2=$value['vk2'];
                $article->vk3=$value['vk3'];
                $article->vk4=$value['vk4'];
                $article->vk5=$value['vk5'];
                $article->vk6=$value['vk6'];
                $article->stock=$value['stock'];
                $article->replenishment_time=$value['replenishment_time'];
                $article->tdp=$value['tdp'];
                $article->available_from=$value['available_from'];
                $article->active_from=$value['active_from'];
                $article->active_until=$value['active_until'];
                $article->puid=$value['puid'];
                $article->pdf_link=$value['pdf_link'];
                $article->length=$value['length'];
                $article->width=$value['width'];
                $article->depth=$value['depth'];
                $article->deeplink=$value['deeplink'];
                $article->long_description_de=$value['long_description_de'];
                $article->long_description_en=$value['long_description_en'];
                $article->save();
            }
          
        }

    }
}
