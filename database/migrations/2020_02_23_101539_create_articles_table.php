<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('article_number');
            $table->string('manufacturer_id');
            $table->string('manufacturer_name');
            $table->string('text1_de')->nullable();
            $table->string('text1_en')->nullable();
            $table->string('sku');
            $table->string('created');
            $table->boolean('shop_status');
            $table->boolean('hidden');
            $table->boolean('deleted');
            $table->float('weight',6,2);
            $table->string('text2_de')->nullable();
            $table->string('text2_en')->nullable();
            $table->string('ean')->nullable();
            $table->float('ek',6,2);
            $table->float('vk1',6,2);
            $table->float('vk2',6,2);
            $table->float('vk3',6,2);
            $table->float('vk4',6,2);
            $table->float('vk5',6,2);
            $table->float('vk6',6,2);
            $table->integer('stock');
            $table->string('replenishment_time');
            $table->string('tdp')->nullable();
            $table->string('available_from')->nullable();
            $table->string('active_from')->nullable();
            $table->string('active_until')->nullable();
            $table->string('puid');
            $table->string('pdf_link')->nullable();
            $table->string('length')->nullable();
            $table->string('width')->nullable();
            $table->string('depth')->nullable();
            $table->string('deeplink');
            $table->text('long_description_de');
            $table->text('long_description_en');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
